<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Mahasiswa - Tambah</title>
</head>
<body>
    <h1>Form Data Siswa</h1>

    <form action="/simpan" method="POST">
        {{ csrf_field() }}
        <label for="Nama Mahasiswa">Nama Mahasiswa</label><br>
        <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Mahasiswa"><br>

        <label for="NIM Mahasiswa">NIM</label><br>
        <input type="text" name="nim" id="nim" class="form-control" placeholder="1915051002"><br>

        <label for="Kelas">Kelas</label><br>
        <input type="text" name="kelas" id="kelas" class="form-control" placeholder="PTI 5A"><br>

        <label for="Program Studi">Program Studi </label><br>
        <input type="text" name="prodi" id="prodi" class="form-control" placeholder="Pendidikan Teknik Informatika"><br>

        <label for="Fakultas">Fakultas </label>
        <input type="text" name="fakultas" id="fakultas" class="form-control" placeholder="Fakultas Teknik dan Kejuruan"><br>

        <input type="submit" name="submit" value="Tambah" class="btn btn-primary">
    </form>
</body>
</html>