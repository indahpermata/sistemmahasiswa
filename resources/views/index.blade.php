<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Data Mahasiswa - Home</title>
</head>
<body>
    <section class="d-flex justify-content-center p-4">
        <h1>Data Mahasiswa</h1>
    </section>

    
    <div class="container">
        <a href="/tambah" class="row btn btn-primary mb-3"> Tambah Data </a>
        <center>
            <table border="1" cellpadding="10" class="table table-striped table-hover">
                <tr class="table-dark">
                    <th>No</th>
                    <th>Nama Mahasiswa</th>
                    <th>NIM</th>
                    <th>kelas</th>
                    <th>Program Studi</th>
                    <th>Fakultas</th>
                    <th>Action</th>
                </tr>
                @foreach ($data as $d)
                    <tr>
                        <td>{{ $d->id }}</td>
                        <td>{{ $d->nama_mahasiswa }}</td>
                        <td>{{ $d->nim_mahasiswa }}</td>
                        <td>{{ $d->kelas_mahasiswa }}</td>
                        <td>{{ $d->prodi_mahasiswa }}</td>
                        <td>{{ $d->fakultas_mahasiswa }}</td>
                        <td>
                            <a href="/ubah/{{$d->id}}">Edit</a>
                            <a href="/hapus/{{$d->id}}">Hapus</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </center>
    </div>
</body>
</html>