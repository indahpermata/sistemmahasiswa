<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class cMahasiswa extends Controller
{
    //read
    public function read()
    {
        $data = DB::table('mahasiswa')->get();
        return view('index', ['data' => $data]);
    }

    //create tampil form simpan data
    public function formadd()
    {
        return view('Formadd');
    }

    //create simpan data
    public function add(Request $add)
    {
        //Masukkan data yang terekam di formulir ke variabel penampung
        $nama = $add->nama;
        $nim = $add->nim;
        $kelas = $add->kelas;
        $prodi = $add->prodi;
        $fakultas = $add->fakultas;

        //Nambah data nama, nim, kelas, prodi, fakultas
        DB::table('mahasiswa')->insert(
            [
                'nama_mahasiswa' => $nama,
                'nim_mahasiswa' => $nim,
                'kelas_mahasiswa' => $kelas,
                'prodi_mahasiswa' => $prodi,
                'fakultas_mahasiswa' => $fakultas
            ]
        );

        //redirect ke halaman utama
        return redirect('/');
    }

    //update tampil form edit
    public function formedit(Int $id)
    {
        $data = DB::table('mahasiswa')->where('id', $id)->first();
        return view('Formedit', ['data' => $data]);
    }

    //update simpan perubahan
    public function edit(Request $data, Int $id)
    {
        $update = DB::table('mahasiswa')->where('id', $id)->update(
            [
                'nama_mahasiswa' => $data->nama,
                'nim_mahasiswa' => $data->nim,
                'kelas_mahasiswa' => $data->kelas,
                'prodi_mahasiswa' => $data->prodi,
                'fakultas_mahasiswa' => $data->fakultas
            ]
            );

            return redirect('/');
    }

    //delete
    public function delete(Int $id)
    {
        $delete = DB::table('mahasiswa')->where('id', $id)->delete();
        return redirect('/');
    }
}
