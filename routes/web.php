<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\cMahasiswa;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Read
Route::get('/', [cMahasiswa::class,'read']);

//Create
Route::get('/tambah', [cMahasiswa::class, 'formadd']);
Route::post('/simpan', [cMahasiswa::class, 'add']);

//update
Route::get('/ubah/{id}', [cMahasiswa::class, 'formedit']);
Route::post('/ubah/{id}', [cMahasiswa::class, 'edit']);

//delete
Route::get('/hapus/{id}', [cMahasiswa::class, 'delete']);